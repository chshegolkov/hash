from tkinter import *

import hashlib
def button_clicked():
    get_text = text1.get('1.0', END)
    get_text_res= bytearray(get_text.replace("\n",""),encoding='utf-8')
    sha = hashlib.sha256(get_text_res).hexdigest()
    text2.delete(1.0, END)
    text2.insert(END, sha)
    
root=Tk()
root.title(u'Gen SHA256')

label1=Label(text="Write Text")
label1.pack()

text1=Text(root,height=3,width=65,font='Arial 14',wrap=WORD)
text1.pack()

label2=Label(text="Result SHA256")
label2.pack()

text2=Text(root,height=3,width=65,font='Arial 14',wrap=WORD)
text2.pack()

button1=Button(root,text='Click on me',width=65,height=3,font='arial 14', command=button_clicked)
button1.pack()

root.mainloop()